package net.sgoliver.android.localizacion;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SerUbicacion  extends Service implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks,
        com.google.android.gms.location.LocationListener {
    private static final int PETICION_CONFIG_UBICACION = 201;
    private static final String LOGTAG = "android-localizacion";
    private LocationRequest locRequest;
    private GoogleApiClient apiClient;
    private DatabaseReference myRef;
    private FirebaseDatabase database;
    private String matricula;

    public SerUbicacion() {
    }

    @Override
/**
 * on create inicializa la BD
 */
    public void onCreate() {
        database = FirebaseDatabase.getInstance();
    }


    @Override
/**
 * Este metodo recibe el intent, genera la apicliente y si la apiclient no esta conectada salta al metodo onConnect
 * si no pasara al metodo enablelocalitationUpdates para empezar a recoger localixzaciones
 */
    public int onStartCommand(Intent intenc, int flags, int idArranque) {

        apiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
        if (!apiClient.isConnected()) {
            apiClient.connect();
        }
        Bundle extras = intenc.getExtras();
        matricula=extras.getString("matricula");
        Log.i("matricula", matricula);
        enableLocationUpdates();

        return START_STICKY;

    }

    /**
     * Recoge las localizaciones para la bd
     */
    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            Log.i(LOGTAG, "Inicio de recepción de ubicaciones");

            LocationServices.FusedLocationApi.requestLocationUpdates(
                    apiClient, locRequest,this);

        }
    }


    /**
     * para la subida a la bd de localizaciones
     */
    public void onDestroy() {
        LocationServices.FusedLocationApi.removeLocationUpdates(apiClient,this);
        super.onDestroy();
    }


    @Override

    public IBinder onBind(Intent intencion) {
        return null;

    }

    /**
     * inicializa la subida de ubicaciones con la secuencia de tiempo de subida
     */
    private void enableLocationUpdates() {
        locRequest = new LocationRequest();
        locRequest.setInterval(5000);
        locRequest.setFastestInterval(2000);
        locRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest locSettingsRequest =
                new LocationSettingsRequest.Builder()
                        .addLocationRequest(locRequest)
                        .build();

    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i("OnConnectionFailed", "Algo ha ido mal");

    }

    /**
     * genera las localizaciones y guarda en la bd
     * @param bundle
     */
    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(apiClient);
        saveLocation(lastLocation);
        Log.i("Conectado", "conectado");
        LocationServices.FusedLocationApi.requestLocationUpdates(apiClient, locRequest,this);
        //enableLocationUpdates();
       //startLocationUpdates();

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e(LOGTAG, "Se ha interrumpido la conexión con Google Play Services");

    }

    @Override
    public void onLocationChanged(Location location) {

        Log.i(LOGTAG, "Recibida nueva ubicación!");
        saveLocation(location);//savelocation me lo traigo

    }

    /**
     * guarda en la bd las localizaciones
     * @param loc
     */
    public void saveLocation (Location loc)
    {
        myRef = database.getReference("root");
        Ubicacion last = new Ubicacion(loc.getLatitude(), loc.getLongitude(), loc.getTime());
        myRef.child(matricula).push().setValue(last);
    }
}
